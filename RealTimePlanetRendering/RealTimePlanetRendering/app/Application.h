#pragma once
#include "common.h"
#include "Camera.h"
#include "AssetsManager.h"
#include "Cube.h"
#include "Sphere.h"
#include "SolarSystem.h"
#include "FpsCounter.h"
#include "Skybox.h"
#include "FrameBufferRenderer.h"

class Application
{
private:
	GLdouble lastTime, deltaTime;
	GLboolean firstMouse, renderWireframe, activePostporcessing;
	GLfloat lastMouseX, lastMouseY;
	FpsCounter fpsCounter;

	unique_ptr<FrameBufferRenderer> frameBufferRenderer;
	shared_ptr<AssetsManager> assets;
	shared_ptr<Camera> camera;
	weak_ptr<Planet> lockedPlanet;

	unique_ptr<Skybox> skybox;
	unique_ptr<SolarSystem> solarSystem;
public:
	Application();

	void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
	void mouseCallback(GLFWwindow* window, GLfloat xpos, GLfloat ypos);
	void update();
	void preprocess();
	void render();
	void postprocess();
};