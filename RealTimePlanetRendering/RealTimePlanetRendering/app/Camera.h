#pragma once
#include "common.h"

const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 2.0f;
const GLfloat SPEED_FAST = 20.0f;
const GLfloat SPEED_SLOW = 0.5f;
const GLfloat SENSITIVTY = 0.06f;

enum class AxisMove { Positive, Negative, None };

enum class CameraSpeed { Normal, Fast, Slow };

class Camera
{
private:
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;
	GLfloat yaw;
	GLfloat pitch;
	GLfloat movementSpeed;
	GLfloat mouseSensitivity;
	GLfloat fov;
	AxisMove frontMove;
	AxisMove upMove;
	AxisMove rightMove;
	glm::vec3 lockPosition;

	void updateVectors();
public:
	Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH);
	Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch);

	void setFrontMove(AxisMove move);
	void setUpMove(AxisMove move);
	void setRightMove(AxisMove move);
	void moveCamera(GLdouble deltaTime);

	void processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true);
	void setCameraSpeed(CameraSpeed speed);

	glm::vec3 getPosition();
	void setPosition(glm::vec3 pos);
	glm::mat4 getViewMatrix();

	void lock(glm::vec3 pos);
	void updateLock(glm::vec3 pos);
	void unlock();
};

