#pragma once
#include "common.h"

class FpsCounter
{
private:
	GLdouble lastTime, lastFrameTime, maxTime;
	GLuint frames;
public:
	FpsCounter();
	void tick();
};