#pragma once
#include "common.h"

class Shader
{
private:
	GLuint id;
	string vertexSource, fragmentSource, gemoetrySource;

	void checkCompileErrors(GLuint object, std::string type);
public:
	Shader(const Shader& other);
	Shader(string vertex, string fragment);

	void compile();
	void use();
	void setUniform1f(string name, GLfloat a);
	void setUniform3f(string name, GLfloat a, GLfloat b, GLfloat c);
	void setUniformMatrix3fv(string name, glm::mat3& mat);
	void setUniformMatrix4fv(string name, glm::mat4& mat);
	void setTextureUnit(string name, GLuint texture, GLuint textureId);
	void setCubeTextureUnit(string name, GLuint texture, GLuint textureId);
	GLuint getAttribute(string name);
};

