#include "FpsCounter.h"

FpsCounter::FpsCounter()
{
	lastTime = lastFrameTime = maxTime = 0.0;
	frames = 0;
}

void FpsCounter::tick()
{
	++frames;
	GLdouble time = glfwGetTime();
	maxTime = max(maxTime, time - lastFrameTime);
	if (lastTime == 0.0)
	{
		lastTime = time;
	}
	else if (time > lastTime + 1.0)
	{
		cout << "FPS: " << frames / (time - lastTime) << ", avg: " << (time - lastTime) / frames << "s, max: " << maxTime << "s" << endl;
		frames = 0;
		lastTime = time;
		maxTime = 0.0;
	}
	lastFrameTime = time;
}