#pragma once
#include <SOIL.h>
#include "common.h"
#include "JuliaSet.h"
#include "MandelBrot.h"
#include "PerlinNoise.h"

class Texture
{
protected:
	int width, height;
	GLuint texture;
public:
	GLint wraping;
	GLint filtering;
	GLint soilFormat;
	GLint internalFormat;
	GLint format;

	Texture();

	void load(string path);
	GLuint getTexture();
};

