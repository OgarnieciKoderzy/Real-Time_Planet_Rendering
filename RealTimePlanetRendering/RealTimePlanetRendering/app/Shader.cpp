#include "Shader.h"

Shader::Shader(const Shader& other) : id(other.id) { }

Shader::Shader(string vertex, string fragment) : vertexSource(vertex), fragmentSource(fragment)
{
}

void Shader::compile()
{
	GLuint vertex, fragment;
	const GLchar* shaderCode;
	// Vertex Shader
	vertex = glCreateShader(GL_VERTEX_SHADER);
	shaderCode = vertexSource.c_str();
	glShaderSource(vertex, 1, &shaderCode, NULL);
	glCompileShader(vertex);
	checkCompileErrors(vertex, "VERTEX");
	// Fragment Shader
	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	shaderCode = fragmentSource.c_str();
	glShaderSource(fragment, 1, &shaderCode, NULL);
	glCompileShader(fragment);
	checkCompileErrors(fragment, "FRAGMENT");
	// Shader Program
	id = glCreateProgram();
	glAttachShader(id, vertex);
	glAttachShader(id, fragment);
	glLinkProgram(id);
	checkCompileErrors(id, "PROGRAM");

	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

void Shader::use()
{
	glUseProgram(id);
}

void Shader::checkCompileErrors(GLuint object, std::string type)
{
	GLint success;
	GLchar infoLog[1024];
	if (type != "PROGRAM")
	{
		glGetShaderiv(object, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << "\n"
				<< infoLog << "\n -- --------------------------------------------------- -- \n";
		}
	}
	else
	{
		glGetProgramiv(object, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(object, 1024, NULL, infoLog);
			std::cout << "| ERROR::Shader: Link-time error: Type: " << type << "\n"
				<< infoLog << "\n -- --------------------------------------------------- -- \n";
		}
	}
}

void Shader::setUniform1f(string name, GLfloat a)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniform1f(loc, a);
}

void Shader::setUniform3f(string name, GLfloat a, GLfloat b, GLfloat c)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniform3f(loc, a, b, c);
}

void Shader::setUniformMatrix3fv(string name, glm::mat3& mat)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniformMatrix3fv(loc, 1, GL_FALSE, glm::value_ptr(mat));
}


void Shader::setUniformMatrix4fv(string name, glm::mat4& mat)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::setTextureUnit(string name, GLuint texture, GLuint textureId)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniform1i(loc, textureId);
	glActiveTexture(GL_TEXTURE0 + textureId);
	glBindTexture(GL_TEXTURE_2D, texture);
}

void Shader::setCubeTextureUnit(string name, GLuint texture, GLuint textureId)
{
	GLint loc = glGetUniformLocation(id, name.c_str());
	glUniform1i(loc, textureId);
	glActiveTexture(GL_TEXTURE0 + textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
}


GLuint Shader::getAttribute(string name)
{
	GLuint loc = glGetAttribLocation(id, name.c_str());
	return loc;
}