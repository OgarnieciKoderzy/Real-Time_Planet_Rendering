#pragma once
#include <sstream>
#include <fstream>
#include <map>
#include "common.h"
#include "Shader.h"
#include "Texture.h"

class AssetsManager
{
private:
	map<string, Shader> shaders;
	map<string, GLuint> textures;

	string loadString(string path);
public:
	AssetsManager();
	~AssetsManager();

	Shader getShader(string name);
	GLuint getTexture(string name);
};

