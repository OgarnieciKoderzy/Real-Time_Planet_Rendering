#include "AssetsManager.h"

AssetsManager::AssetsManager()
{
	string vertexShader;
	string fragmentShader;

	vertexShader = loadString("shaders\\triangle.vert");
	fragmentShader = loadString("shaders\\triangle.frag");
	Shader triangle(vertexShader, fragmentShader);
	triangle.compile();
	shaders.insert(make_pair("triangle", triangle));

	vertexShader = loadString("shaders\\lamp.vert");
	fragmentShader = loadString("shaders\\lamp.frag");
	Shader lamp(vertexShader, fragmentShader);
	lamp.compile();
	shaders.insert(make_pair("lamp", lamp));

	vertexShader = loadString("shaders\\phong.vert");
	fragmentShader = loadString("shaders\\phong.frag");
	Shader phong(vertexShader, fragmentShader);
	phong.compile();
	shaders.insert(make_pair("phong", phong));

	vertexShader = loadString("shaders\\sun.vert");
	fragmentShader = loadString("shaders\\sun.frag");
	Shader sun(vertexShader, fragmentShader);
	sun.compile();
	shaders.insert(make_pair("sun", sun));

	vertexShader = loadString("shaders\\terrain.vert");
	fragmentShader = loadString("shaders\\terrain.frag");
	Shader terrain(vertexShader, fragmentShader);
	terrain.compile();
	shaders.insert(make_pair("terrain", terrain));

	vertexShader = loadString("shaders\\normal.vert");
	fragmentShader = loadString("shaders\\normal.frag");
	Shader normal(vertexShader, fragmentShader);
	normal.compile();
	shaders.insert(make_pair("normal", normal));

	vertexShader = loadString("shaders\\skybox.vert");
	fragmentShader = loadString("shaders\\skybox.frag");
	Shader skybox(vertexShader, fragmentShader);
	skybox.compile();
	shaders.insert(make_pair("skybox", skybox));

	vertexShader = loadString("shaders\\screen.vert");
	fragmentShader = loadString("shaders\\screen.frag");
	Shader screen(vertexShader, fragmentShader);
	screen.compile();
	shaders.insert(make_pair("screen", screen));


	Texture texture;
	texture.load("textures\\Perlin.png");
	textures["perlin"] = texture.getTexture();
	texture.load("textures\\raw2d.png");
	textures["raw2d"] = texture.getTexture();
	texture.load("textures\\noise.png");
	textures["noise"] = texture.getTexture();
	//vector<string> textureNames = { "textures\\back.png", "textures\\front.png", "textures\\right.png", "textures\\left.png", "textures\\top.png", "textures\\bottom.png" };
}

AssetsManager::~AssetsManager()
{
}

string AssetsManager::loadString(string path)
{
	string str;
	try
	{
		std::ifstream file(path);
		std::stringstream stringStream;
		stringStream << file.rdbuf();
		file.close();
		str = stringStream.str();
	}
	catch (std::exception e)
	{
		cout << "ERROR::SHADER: Failed to read shader files" << endl;
	}
	return str;
}

Shader AssetsManager::getShader(string name)
{
	auto it = shaders.find(name);
	if (it == shaders.end())
	{
		cout << "ERROR::ASSETS: Coudn't find shader " << name << endl;
	}
	return it->second;
}

GLuint AssetsManager::getTexture(string name)
{
	auto it = textures.find(name);
	if (it == textures.end())
	{
		cout << "ERROR::ASSETS: Coudn't find texture " << name << endl;
	}
	return it->second;
}
