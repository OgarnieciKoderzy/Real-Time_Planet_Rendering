#include "Camera.h"

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch)
{
	front = glm::vec3(0.0f, 0.0f, -1.0f);
	movementSpeed = SPEED;
	mouseSensitivity = SENSITIVTY;
	fov = FOV;
	frontMove = AxisMove::None;
	upMove = AxisMove::None;
	rightMove = AxisMove::None;
	this->position = position;
	this->worldUp = up;
	this->yaw = yaw;
	this->pitch = pitch;
	updateVectors();
}

Camera::Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch)
{
	front = glm::vec3(0.0f, 0.0f, -1.0f);
	movementSpeed = SPEED;
	mouseSensitivity = SENSITIVTY;
	fov = FOV;
	frontMove = AxisMove::None;
	upMove = AxisMove::None;
	rightMove = AxisMove::None;
	this->position = glm::vec3(posX, posY, posZ);
	this->worldUp = glm::vec3(upX, upY, upZ);
	this->yaw = yaw;
	this->pitch = pitch;
	updateVectors();
}

void Camera::updateVectors()
{
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(front);
	
	right = glm::normalize(glm::cross(front, worldUp));
	up = glm::normalize(glm::cross(right, front));
}

glm::vec3 Camera::getPosition()
{
	return position + lockPosition;
}

void Camera::setPosition(glm::vec3 pos)
{
	position = pos - lockPosition;
}

glm::mat4 Camera::getViewMatrix()
{
	return glm::lookAt(getPosition(), getPosition() + front, up);
}

void Camera::setFrontMove(AxisMove move)
{
	frontMove = move;
}

void Camera::setUpMove(AxisMove move)
{
	upMove = move;
}

void Camera::setRightMove(AxisMove move)
{
	rightMove = move;
}

void Camera::moveCamera(GLdouble deltaTime)
{
	GLfloat modifier = min(glm::length(position) * 0.02f, 10.0f);
	GLfloat delta = (GLfloat)(movementSpeed * modifier * deltaTime);
	if (frontMove == AxisMove::Positive)
		position += front * delta;
	else if (frontMove == AxisMove::Negative)
		position -= front * delta;
	if (upMove == AxisMove::Positive)
		position += up * delta;
	else if (upMove == AxisMove::Negative)
		position -= up * delta;
	if (rightMove == AxisMove::Positive)
		position += right * delta;
	else if (rightMove == AxisMove::Negative)
		position -= right * delta;
}

void Camera::processMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch)
{
	xoffset *= mouseSensitivity;
	yoffset *= mouseSensitivity;
	yaw += xoffset;
	pitch += yoffset;
	if (constrainPitch)
	{
		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;
	}
	this->updateVectors();
}

void Camera::setCameraSpeed(CameraSpeed speed)
{
	switch (speed)
	{
	case CameraSpeed::Fast:
		movementSpeed = SPEED_FAST;
		break;
	case CameraSpeed::Slow:
		movementSpeed = SPEED_SLOW;
		break;
	default:
		movementSpeed = SPEED;
		break;
	}
}

void Camera::lock(glm::vec3 pos)
{
	unlock();
	position -= pos;
	lockPosition = pos;
}

void Camera::updateLock(glm::vec3 pos)
{
	lockPosition = pos;
}

void Camera::unlock()
{
	position += lockPosition;
	lockPosition = glm::vec3(0, 0, 0);
}