#include <memory>
#include <string>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

using namespace std;

#define M_PI 3.141592653589793

#define WIDTH 1024
#define HEIGHT 768
#define FOV 45.0f
#define Z_NEAR 0.1f
#define Z_FAR 10000.0f