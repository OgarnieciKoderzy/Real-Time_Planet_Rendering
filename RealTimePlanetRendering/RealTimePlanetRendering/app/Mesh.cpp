#include "Mesh.h"


Mesh::Mesh(Shader shader) : shader(shader), scale(1.0f)
{
	vao = vbo = ebo = 0;
}

Mesh::~Mesh()
{
	if (vao != 0)
		glDeleteVertexArrays(1, &vao);
	if (vbo != 0)
		glDeleteBuffers(1, &vbo);
	if (ebo != 0)
		glDeleteBuffers(1, &ebo);
}

void Mesh::setPosition(glm::vec3 pos)
{
	position = pos;
}

glm::vec3 Mesh::getPosition()
{
	return position;
}

void Mesh::setScale(glm::vec3 s)
{
	scale = s;
}

glm::vec3 Mesh::getScale()
{
	return scale;
}