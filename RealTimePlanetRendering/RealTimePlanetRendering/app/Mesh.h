#pragma once
#include "common.h"
#include "Shader.h"
#include "Camera.h"

class Mesh
{
protected:
	GLuint vbo, vao, ebo;
	Shader shader;
	glm::vec3 position;
	glm::vec3 scale;
public:
	Mesh(Shader shader);
	virtual ~Mesh();

	virtual void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition) = 0;

	void setPosition(glm::vec3 pos);
	glm::vec3 getPosition();
	void setScale(glm::vec3 s);
	glm::vec3 getScale();
};

