#include "Application.h"
GLfloat abc = 0;
const GLfloat orbit = 100.0f;

Application::Application()
{
	srand((unsigned)time(0));

	camera = make_shared<Camera>(glm::vec3(0.0f, 10.0f, 120.0f));
	assets = make_shared<AssetsManager>();

	frameBufferRenderer = make_unique<FrameBufferRenderer>(assets->getShader("screen"));

	skybox = make_unique<Skybox>(assets->getShader("skybox"));
	solarSystem = make_unique<SolarSystem>(assets);

	lastTime = glfwGetTime();
	firstMouse = false;
	renderWireframe = false;
	activePostporcessing = true;
}

void Application::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	switch (key)
	{
	case GLFW_KEY_ESCAPE:
		if (action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
		break;
	case GLFW_KEY_W:
		if (action == GLFW_PRESS)
			camera->setFrontMove(AxisMove::Positive);
		else if (action == GLFW_RELEASE)
			camera->setFrontMove(AxisMove::None);
		break;
	case GLFW_KEY_S:
		if (action == GLFW_PRESS)
			camera->setFrontMove(AxisMove::Negative);
		else if (action == GLFW_RELEASE)
			camera->setFrontMove(AxisMove::None);
		break;
	case GLFW_KEY_D:
		if (action == GLFW_PRESS)
			camera->setRightMove(AxisMove::Positive);
		else if (action == GLFW_RELEASE)
			camera->setRightMove(AxisMove::None);
		break;
	case GLFW_KEY_A:
		if (action == GLFW_PRESS)
			camera->setRightMove(AxisMove::Negative);
		else if (action == GLFW_RELEASE)
			camera->setRightMove(AxisMove::None);
		break;
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS)
			camera->setUpMove(AxisMove::Positive);
		else if (action == GLFW_RELEASE)
			camera->setUpMove(AxisMove::None);
		break;
	case GLFW_KEY_LEFT_CONTROL:
		if (action == GLFW_PRESS)
			camera->setUpMove(AxisMove::Negative);
		else if (action == GLFW_RELEASE)
			camera->setUpMove(AxisMove::None);
		break;
	case GLFW_KEY_X:
		if (action == GLFW_PRESS)
			renderWireframe = !renderWireframe;
		break;
	case GLFW_KEY_LEFT_SHIFT:
		if (action == GLFW_PRESS)
			camera->setCameraSpeed(CameraSpeed::Fast);
		else if (action == GLFW_RELEASE)
			camera->setCameraSpeed(CameraSpeed::Normal);
		break;
	case GLFW_KEY_C:
		if (action == GLFW_PRESS)
			camera->setCameraSpeed(CameraSpeed::Slow);
		else if (action == GLFW_RELEASE)
			camera->setCameraSpeed(CameraSpeed::Normal);
		break;
	case GLFW_KEY_L:
		if (action == GLFW_PRESS)
		{
			lockedPlanet = solarSystem->getClosestPlanet(camera->getPosition());
			if (auto planet = lockedPlanet.lock())
				camera->lock(planet->getPosition());
		}
		break;
	case GLFW_KEY_U:
		if (action == GLFW_PRESS)
		{
			lockedPlanet.reset();
			camera->unlock();
		}
		break;
	case GLFW_KEY_P:
		if (action == GLFW_PRESS)
			activePostporcessing = !activePostporcessing;
		break;
	}
}

void Application::mouseCallback(GLFWwindow* window, GLfloat xpos, GLfloat ypos)
{
	if (firstMouse)
	{
		lastMouseX = xpos;
		lastMouseY = ypos;
		firstMouse = false;
	}
	GLfloat xoffset = xpos - lastMouseX;
	GLfloat yoffset = lastMouseY - ypos;
	lastMouseX = xpos;
	lastMouseY = ypos;
	camera->processMouseMovement(xoffset, yoffset);
}

void Application::update()
{
	GLdouble time = glfwGetTime();
	deltaTime = time - lastTime;
	lastTime = time;

	solarSystem->update(deltaTime);

	if (auto planet = lockedPlanet.lock())
		camera->updateLock(planet->getPosition());
	camera->moveCamera(deltaTime);
	solarSystem->checkCollision(camera);
}

void Application::render()
{
	skybox->draw(camera);
	if (renderWireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	solarSystem->draw(camera);
	if (renderWireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	fpsCounter.tick();
}

void Application::preprocess()
{
	if (activePostporcessing)
		frameBufferRenderer->bind();
}

void Application::postprocess()
{
	if (frameBufferRenderer->isBound())
		frameBufferRenderer->render(camera, deltaTime);
}
