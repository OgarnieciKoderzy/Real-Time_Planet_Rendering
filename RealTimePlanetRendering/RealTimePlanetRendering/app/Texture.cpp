#include "Texture.h"

Texture::Texture()
{
	texture = 0;
	wraping = GL_CLAMP_TO_EDGE;
	filtering = GL_LINEAR;
	soilFormat = SOIL_LOAD_RGB;
	internalFormat = GL_RGB;
	format = GL_RGB;
}

void Texture::load(string path)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wraping);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wraping);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);

	unsigned char* image = SOIL_load_image(path.c_str(), &width, &height, 0, soilFormat);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::getTexture()
{
	return texture;
}
