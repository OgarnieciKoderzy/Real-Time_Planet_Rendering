#version 330 core

in vec2 TexCoords;
out vec4 color;
 
uniform sampler2D colorTexture;
uniform sampler2D depthTexture;
uniform mat4 viewProjectionInverse;
uniform mat4 previousViewProjection;
uniform float deltaTime;

const int numSamples = 5;
 
void main()
{
	float zOverW = texture(depthTexture, TexCoords).r;
	vec4 H = vec4(TexCoords.x * 2.0 - 1.0, (1.0 - TexCoords.y) * 2.0 - 1.0, zOverW, 1.0);
	vec4 D = H * viewProjectionInverse;
	vec4 worldPos = D / D.w;
	vec4 currentPos = H;
	vec4 previousPos = worldPos * previousViewProjection;
	previousPos /= previousPos.w;  
	vec2 velocity = (currentPos - previousPos).xy;
	velocity *= deltaTime;

	vec3 col = texture(colorTexture, TexCoords).rgb;
	vec2 texCoord = TexCoords + velocity;
	int samples;
	for(samples = 1; samples < numSamples && texCoord.x <= 1.0 && texCoord.x >= 0.0 && texCoord.y <= 1.0 && texCoord.y >= 0.0; ++samples, texCoord += velocity)
	{
		vec3 currentColor = texture(colorTexture, texCoord).rgb;
		col += currentColor;  
	}
	color = vec4(col / samples, 1.0);
}
