#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 faceCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float heightScale;

uniform sampler2D heightMap;

out vec3 fragPosition;
out vec2 texCoord;

vec3 getPosition(vec3 pos, vec2 coords)
{
	float v = texture(heightMap, coords).r;
	float scale = (1.0 - heightScale / 2) + v * heightScale;
	return pos * scale;
}

void main()
{
	vec3 pos = getPosition(position, faceCoord);

    gl_Position = projection * view * model * vec4(pos, 1.0f);
	fragPosition = vec3(model * vec4(pos, 1.0f));
	texCoord = faceCoord;
}