#version 330 core

in vec2 uv1;
in vec2 st1;

out vec4 color;

uniform float sampleDistance;
uniform float invertedSampleDistance;
uniform mat3 faceTransform;
uniform float heightScale;

uniform sampler2D heightMap;

void main() {	
	vec2 xOffset = sampleDistance * vec2(1, 0);
	vec2 yOffset = sampleDistance * vec2(0, 1);
	
	float xDifference = texture2D(heightMap, uv1 + xOffset).r - texture2D(heightMap, uv1 - xOffset).r;
	float yDifference = texture2D(heightMap, uv1 + yOffset).r - texture2D(heightMap, uv1 - yOffset).r;
					  
	float s = st1.x;
	float t = st1.y;
	float iw = inversesqrt(s * s + t * t + 1.0);
	float h = 1.0 + heightScale * texture2D(heightMap, uv1).r;
	
	float st = s * t;
	float iw2 = iw * iw;
	float iw3 = iw2 * iw;
	float hiw = h * iw;
	float hiw3 = h * iw3;
	
	mat3 jacobian = mat3(
		hiw * (1.0 - s * s * iw2), -st * hiw3, -s * hiw3,
		-st * hiw3, hiw * (1.0 - t * t * iw2), -t * hiw3,
		heightScale * s * iw, heightScale * t * iw, heightScale * iw
	);
	
	vec3 normal = normalize(
		faceTransform * cross(
			jacobian * vec3(2, 0,  xDifference * invertedSampleDistance),
			jacobian * vec3(0, 2, -yDifference * invertedSampleDistance)
		)
	);
	
	color = vec4(normal * 0.5 + 0.5, 1);
}