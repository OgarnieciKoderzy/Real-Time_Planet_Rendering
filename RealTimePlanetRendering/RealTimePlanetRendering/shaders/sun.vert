#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoord;

uniform mat4 projection;
uniform mat4 modelView;

out vec2 fragCoord;
 
void main()
{
    gl_Position = projection * modelView * vec4(position, 0.0, 1.0);
	fragCoord = texCoord;
}