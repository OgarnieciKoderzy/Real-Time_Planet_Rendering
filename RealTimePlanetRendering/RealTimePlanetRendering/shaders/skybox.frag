#version 400 core

in vec3 direction;
out vec4 color;

uniform sampler2D sam;

vec3 cube(in vec3 d, in float s, in float b)
{
    vec3 colx = texture(sam, 0.5 + s*d.yz/d.x, b).xyz;
    vec3 coly = texture(sam, 0.5 + s*d.zx/d.y, b).xyz;
    vec3 colz = texture(sam, 0.5 + s*d.xy/d.z, b).xyz;
    vec3 n = d*d;
    return (colx*n.x + coly*n.y + colz*n.z)/(n.x+n.y+n.z);
}

vec2 hash(vec2 p)
{ 
	p = vec2(dot(p, vec2(127.1, 311.7)), dot(p, vec2(269.5, 183.3)));
	return fract(sin(p) * 43758.5453); 
}

vec2 voronoi(in vec2 x)
{
    vec2 n = floor(x);
    vec2 f = fract(x);

	vec3 m = vec3(8.0);
    for(int j=-1; j<=1; j++)
    for(int i=-1; i<=1; i++)
    {
        vec2 g = vec2(float(i), float(j));
        vec2 o = hash(n + g);
        vec2 r = g - f + o;
		float d = dot(r, r);
        if(d < m.x) m = vec3(d, o);
    }

    return vec2(sqrt(m.x), m.y + m.z);
}

vec3 background(in vec3 d)
{
    vec3 col = vec3(0.0);
    col += 0.5 * pow(cube(d, 0.05, 5.0).zyx, vec3(2.0));
    col += 0.2 * pow(cube(d, 0.10, 3.0).zyx, vec3(1.5));
    col += 0.8 * vec3(0.80, 0.5, 0.6) * pow(cube(d, 0.1, 0.0).xxx, vec3(6.0));
    float stars = smoothstep(0.3, 0.7, cube(d, 0.91, 0.0).x);

    vec3 n = abs(d);
    n = n*n*n;
    vec2 vxy = voronoi(50.0*d.xy);
    vec2 vyz = voronoi(50.0*d.yz);
    vec2 vzx = voronoi(50.0*d.zx);
    vec2 r = (vyz*n.x + vzx*n.y + vxy*n.z) / (n.x + n.y + n.z);
    col += 0.9 * stars * clamp(1.0 - (3.0 + r.y*5.0) * r.x, 0.0, 1.0);

    col = 1.5 * col - 0.2;
    col += vec3(-0.05, 0.1, 0.0);
    
    return col;
}

void main()
{
    color = vec4(background(direction), 1.0);
} 