#version 330 core

in vec3 fragPosition;
in vec2 texCoord;

out vec4 color;

uniform sampler2D heightMap;
uniform sampler2D normalMap;

uniform vec3 lightPos; 
uniform vec3 viewPos;
uniform mat4 model;
uniform vec3 firstColor;
uniform vec3 secondColor;
uniform vec3 thirdColor;
uniform vec3 fourthColor;
uniform float firstStep;
uniform float secondStep;

const vec3 lightColor = vec3(1.0, 1.0, 1.0);
const float ambientStrength = 0.1f;

void main()
{
	float r = texture(heightMap, texCoord).r;
	vec3 objectColor;
	if (r < firstStep) objectColor = mix(firstColor, secondColor, r / firstStep);
	else if (r < secondStep) objectColor = mix(secondColor, thirdColor, (r - firstStep) / (secondStep - firstStep));
	else objectColor = mix(thirdColor, fourthColor, (r - secondStep) / (1.0 - secondStep));

    vec3 ambient = ambientStrength * lightColor;
  	
	vec3 norm = normalize((texture(normalMap, texCoord).rgb - 0.5) * 2.0);
    vec3 lightDir = normalize(lightPos - fragPosition);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
        
    vec3 result = (ambient + diffuse) * objectColor;
	color = vec4(result, 1.0f);
} 