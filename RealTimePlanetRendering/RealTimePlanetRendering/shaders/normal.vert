#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 coord1;
layout (location = 2) in vec2 coord2;

out vec2 uv1;
out vec2 st1;

void main()
{
    gl_Position = vec4(position.x, position.y, 0.0, 1.0);
	uv1 = coord1;
	st1 = coord2;
}