#pragma once
#include "Mesh.h"

class Triangle : public Mesh
{
public:
	Triangle(Shader shader);

	void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition) override;
};

