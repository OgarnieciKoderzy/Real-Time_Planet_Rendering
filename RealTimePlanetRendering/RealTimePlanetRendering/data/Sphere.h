#pragma once
#include <vector>
#include "Mesh.h"

class Sphere : public Mesh
{
private:
	GLuint vertexCount;
public:
	Sphere(Shader shader, GLuint resolution);

	void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition) override;
};