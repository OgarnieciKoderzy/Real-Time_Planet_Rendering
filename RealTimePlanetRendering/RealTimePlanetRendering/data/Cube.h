#pragma once
#include "Mesh.h"

class Cube : public Mesh
{
public:
	Cube(Shader shader);
	
	void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition) override;
};

