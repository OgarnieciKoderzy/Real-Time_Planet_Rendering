#include "Sphere.h"

Sphere::Sphere(Shader shader, GLuint resolution) : Mesh(shader)
{
	vector<GLfloat> vertices;
	auto buildWall = [resolution, &vertices](GLuint wall) {
		GLfloat chunk = 2.0f / resolution;
		GLfloat tmpa[6], tmpb[6];
		for (GLuint i = 0; i < resolution; ++i)
		{
			for (GLuint j = 0; j < resolution; ++j)
			{
				tmpa[0] = chunk * i - 1.0f;
				tmpb[0] = chunk * j - 1.0f;
				tmpa[1] = chunk * i - 1.0f;
				tmpb[1] = chunk * (j + 1) - 1.0f;
				tmpa[2] = chunk * (i + 1) - 1.0f;
				tmpb[2] = chunk * (j + 1) - 1.0f;
				tmpa[3] = chunk * i - 1.0f;
				tmpb[3] = chunk * j - 1.0f;
				tmpa[4] = chunk * (i + 1) - 1.0f;
				tmpb[4] = chunk * (j + 1) - 1.0f;
				tmpa[5] = chunk * (i + 1) - 1.0f;
				tmpb[5] = chunk * j - 1.0f;
				for (GLuint k = 0; k < 6; ++k)
				{
					switch (wall)
					{
					case 0:
						vertices.push_back(tmpa[k]);
						vertices.push_back(tmpb[k]);
						vertices.push_back(-1.0f);
						break;
					case 1:
						vertices.push_back(-1.0f);
						vertices.push_back(tmpb[k]);
						vertices.push_back(tmpa[k]);
						break;
					case 2:
						vertices.push_back(tmpa[k]);
						vertices.push_back(tmpb[k]);
						vertices.push_back(1.0f);
						break;
					case 3:
						vertices.push_back(1.0f);
						vertices.push_back(tmpb[k]);
						vertices.push_back(tmpa[k]);
						break;
					case 4:
						vertices.push_back(tmpa[k]);
						vertices.push_back(1.0f);
						vertices.push_back(tmpb[k]);
						break;
					case 5:
						vertices.push_back(tmpa[k]);
						vertices.push_back(-1.0f);
						vertices.push_back(tmpb[k]);
						break;
					}
				}
			}
		}
		return;
	};

	for (GLuint i = 0; i < 6; ++i)
		buildWall(i);

	for (GLuint i = 0; i < vertices.size(); i += 3)
	{
		GLfloat x = vertices[i];
		GLfloat y = vertices[i + 1];
		GLfloat z = vertices[i + 2];
		vertices[i] = x * sqrtf(1.0f - (y*y / 2.0f) - (z*z / 2.0f) + (y*y*z*z / 3.0f));
		vertices[i + 1] = y * sqrtf(1.0f - (z*z / 2.0f) - (x*x / 2.0f) + (z*z*x*x / 3.0f));
		vertices[i + 2] = z * sqrtf(1.0f - (x*x / 2.0f) - (y*y / 2.0f) + (x*x*y*y / 3.0f));
	}

	GLfloat* data = vertices.data();
	vertexCount = vertices.size() / 3;
	
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), data, GL_STATIC_DRAW);

	glBindVertexArray(vao);
	// Positions
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	//// Normals
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	//glEnableVertexAttribArray(1);
	glBindVertexArray(0);
}

void Sphere::draw(shared_ptr<Camera> camera, glm::vec3 lightPosition)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glm::vec3 cameraPos = camera->getPosition();
	glm::mat4 model;
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	glm::mat4 view = camera->getViewMatrix();
	glm::mat4 projection = glm::perspective(FOV, (GLfloat)WIDTH / (GLfloat)HEIGHT, Z_NEAR, Z_FAR);

	shader.use();
	shader.setUniform3f("objectColor", 1.0f, 0.5f, 0.31f);
	shader.setUniform3f("lightColor", 1.0f, 1.0f, 1.0f);
	shader.setUniform3f("lightPos", lightPosition.x, lightPosition.y, lightPosition.z);
	shader.setUniform3f("viewPos", cameraPos.x, cameraPos.y, cameraPos.z);
	shader.setUniformMatrix4fv("model", model);
	shader.setUniformMatrix4fv("view", view);
	shader.setUniformMatrix4fv("projection", projection);

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	glBindVertexArray(0);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}
