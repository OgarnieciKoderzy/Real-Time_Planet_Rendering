#include "HeightMapTexture.h"


HeightMapTexture::HeightMapTexture(double Re, double Im, int w, int h)
{
	width = w;
	height = h;
	JuliaSet jSet(height, width, Re, Im, (std::rand() % 5) + 1 , (double)rand() / RAND_MAX, (double)rand() / RAND_MAX, 256, rand() / (float)RAND_MAX * 10.0f);
	PerlinNoise pSet(rand(), height, width);
	MandelBrot mSet(height, width, (rand() % 50) + 50, rand() / (float)RAND_MAX * 10.0f);
	pSet.overlay(jSet.getMap(), rand() / (float) RAND_MAX * 20.0f);
	pSet.overlay(mSet.getMap(), rand() / (float) RAND_MAX);
	pSet.normalizeEdges();


	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wraping);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wraping);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, pSet.getHeight(), pSet.getWidth(), 0, format, GL_UNSIGNED_BYTE, &pSet.getMap()[0]);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}