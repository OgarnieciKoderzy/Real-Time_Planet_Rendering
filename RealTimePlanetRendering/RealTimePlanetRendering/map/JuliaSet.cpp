#include "JuliaSet.h"



JuliaSet::JuliaSet(int height, int width, double Re, double Img, double mul, double moveX, double moveY, int maxIterations, float radius): Map(height,width) {
	double newRe, newIm, oldRe, oldIm;
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++)
		{
			newRe = 1.5 * (x - width / 2) / (0.5 * mul * width) + moveX;
			newIm = (y - height / 2) / (0.5 * mul * height) + moveY;
			int i;
			for (i = 0; i < maxIterations; i++)
			{
				oldRe = newRe;
				oldIm = newIm;

				newRe = oldRe * oldRe - oldIm * oldIm + Re;
				newIm = 2 * oldRe * oldIm + Img;
				// radius > 2 
				if ((newRe * newRe + newIm * newIm) > radius) break;
			}
			this->map.push_back(i);
			this->map.push_back(i);
			this->map.push_back(i);

		}
	}

}
