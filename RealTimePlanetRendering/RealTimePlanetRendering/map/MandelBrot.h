#pragma once
#include "Map.h"

class MandelBrot: public Map {
public:
	MandelBrot(int height, int width, int maxIterations, float radius);
};