#pragma once
#include "Map.h"
class JuliaSet: public Map {
public:
	JuliaSet(int height, int width, double Re, double Img, double mul, double moveX, double moveY, int maxIterations, float radius);
};