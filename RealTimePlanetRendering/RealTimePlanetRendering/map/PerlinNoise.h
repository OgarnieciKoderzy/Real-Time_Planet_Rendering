#pragma once
#include <vector>
#include "Map.h"

//source http://mrl.nyu.edu/~perlin/noise/)

class PerlinNoise : public Map{
	std::vector<int> p;
	double noise(double x, double y, double z);
	double fade(double t);
	double lerp(double t, double a, double b);
	double grad(int hash, double x, double y, double z);
	public:
	PerlinNoise(int seed, int height, int width);

};
