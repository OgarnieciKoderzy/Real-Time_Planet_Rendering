#include "MandelBrot.h"
MandelBrot::MandelBrot(int height, int width, int maxIterations, float radius) : Map(height, width){
	for (int x = 0;x < width; x++) {
		for (int y = 0;y < height; y++) {
			float re = (float)x / width;
			float im = (float)y / height;
			float tempRe = 0.0;
			int i;
			for (i = 0; i < maxIterations; i++) {
				tempRe = re*re - im*im + (float)x / width;
				im = re * im * 2 + (float)y / height;
				re = tempRe;
				if ((re*re + im*im) > radius) {
					break;
				}
			}
			this->map.push_back(i);
			this->map.push_back(i);
			this->map.push_back(i);
		}
	}
}
