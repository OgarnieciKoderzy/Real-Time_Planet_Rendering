#pragma once
#include <vector>
#include <algorithm>
#include <cmath>

class Map {
protected:
	std::vector<unsigned char> map;
	int height;
	int width;
public:
	Map(int height, int width) :height(height), width(width) {}

	void overlay(std::vector<unsigned char> map, float ratio);
	void normalizeEdges();

	int getHeight() { return this->height; }
	int getWidth() { return this->width; }
	std::vector<unsigned char> getMap() { return this->map; }
};