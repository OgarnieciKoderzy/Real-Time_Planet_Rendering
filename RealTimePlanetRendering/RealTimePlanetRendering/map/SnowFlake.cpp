#include "SnowFlake.h"
void SnowFlake::generateSegment(Point p0, Point p1, int depth) {
	if (depth <= 0) {

		return;
	}
	else {
		Point p1_3, p2_3, p_top;
		// first turn-point
		p1_3.x = p0.x + (p1.x - p0.x) / 3;
		p1_3.y = p0.y + (p1.y - p0.y) / 3;
		// second turn-point
		p2_3.x = p1.x - (p1.x - p0.x) / 3;
		p2_3.y = p1.y - (p1.y - p0.y) / 3;
		// triangle top
		p_top.x = (p0.x + p1.x) / 2.0 - sqrt(3.0) / 6.0 *(p1.y - p0.y);
		p_top.y = (p0.y + p1.y) / 2.0 + sqrt(3.0) / 6.0 *(p1.x - p0.x);

		generateSegment(p0, p1_3, depth - 1);
		generateSegment(p1_3, p_top, depth - 1);
		generateSegment(p_top, p2_3, depth - 1);
		generateSegment(p2_3, p1, depth - 1);

	}
}