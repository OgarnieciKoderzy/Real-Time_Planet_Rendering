#pragma once
#include <math.h>
class SnowFlake {
private:
	struct Point {
		float x;
		float y;
	};
	void generateSegment(Point p0, Point p1, int depth);
};