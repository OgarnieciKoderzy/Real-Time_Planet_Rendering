#include "Map.h"

#define M_PI 3.14159265358979323846

void Map::overlay(std::vector<unsigned char> map, float ratio) {
	int max = map.size() < this->map.size() ? map.size() : this->map.size();
	for (int i = 0; i < max; i++) {
		this->map[i] += (ratio *map[i]);
	}
}

void Map::normalizeEdges()
{
	auto getIndex = [this](int x, int y) {
		return x * 3 + y * width * 3;
	};
	int level = 100;
	int steps = 30;
	int step = std::max(level, 256 - level) / steps;
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			int d = std::min(std::min(x, width - x - 1), std::min(y, height - y - 1));
			if (d < steps)
			{
				float f = sin((M_PI / 2) * d / steps) * (step * steps);
				int i = getIndex(x, y);
				if (map[i] < level - f) map[i] = level - f;
				else if (map[i] > level + f) map[i] = level + f;
			}
		}
	}
}