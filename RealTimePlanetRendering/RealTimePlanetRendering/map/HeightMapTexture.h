#pragma once
#include "Texture.h"

class HeightMapTexture : public Texture
{
public:
	HeightMapTexture(double Re, double Im, int width, int height);
};

