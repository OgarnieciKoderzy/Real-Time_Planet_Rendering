#pragma once
#include "common.h"
#include "Shader.h"
#include "Camera.h"

class FrameBufferRenderer
{
private:
	GLuint framebuffer;
	GLuint textureColorbuffer;
	GLuint textureDepthbuffer;
	GLuint quadVAO;
	Shader shader;
	GLboolean bound;
	glm::mat4 previousViewProjection;

public:
	FrameBufferRenderer(Shader shader);
	~FrameBufferRenderer();

	void bind();
	void render(shared_ptr<Camera> camera, GLfloat deltaTime);
	GLboolean isBound();
};