#include "common.h"
#include "Application.h"

unique_ptr<Application> app;

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	app->keyCallback(window, key, scancode, action, mode);
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	app->mouseCallback(window, (GLfloat)xpos, (GLfloat)ypos);
}

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Real - time Planet Rendering", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;
	glewInit();

	glViewport(0, 0, WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// V-sync: 0-off, 1-on
	//glfwSwapInterval(0);

	// Anti-aliasing
	//glfwWindowHint(GLFW_SAMPLES, 4);

	glEnable(GL_CULL_FACE);

	cout << "Initializing..." << endl;
	app = make_unique<Application>();
	cout << "Initialized." << endl;

	while(!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		app->update();
		app->preprocess();
		app->render();
		app->postprocess();
		glfwSwapBuffers(window);
	}

	app.reset();

	glfwTerminate();
	return 0;
}