#include "QuadtreeNode.h"

QuadtreeNode::QuadtreeNode(GLuint resolution, GLuint chunkResolution, GLuint level, GLuint xoff, GLuint yoff, vector<GLuint>& indices, vector<glm::vec3>& vertices, vector<glm::vec2>& textureCoords) : lodLevel(level)
{
	GLuint chunkGap = 1 << level;
	vector<GLfloat> chunkVertices;
	auto getId = [xoff, yoff, resolution, chunkGap](GLuint i, GLuint j) {
		return (i * chunkGap + yoff) * (resolution + 1) + j * chunkGap + xoff;
	};
	for (GLuint i = 0; i <= chunkResolution; ++i)
	{
		for (GLuint j = 0; j <= chunkResolution; ++j)
		{
			GLuint id = getId(i, j);
			chunkVertices.push_back(vertices[id].x);
			chunkVertices.push_back(vertices[id].y);
			chunkVertices.push_back(vertices[id].z);
			chunkVertices.push_back(textureCoords[id].x);
			chunkVertices.push_back(textureCoords[id].y);
		}
	}
	auto makeSpherical = [](glm::vec3& v) {
		GLfloat dx = v.x * sqrtf(1.0f - (v.y*v.y / 2.0f) - (v.z*v.z / 2.0f) + (v.y*v.y*v.z*v.z / 3.0f));
		GLfloat dy = v.y * sqrtf(1.0f - (v.z*v.z / 2.0f) - (v.x*v.x / 2.0f) + (v.z*v.z*v.x*v.x / 3.0f));
		GLfloat dz = v.z * sqrtf(1.0f - (v.x*v.x / 2.0f) - (v.y*v.y / 2.0f) + (v.x*v.x*v.y*v.y / 3.0f));
		return glm::vec3(dx, dy, dz);
	};
	boundVertices.push_back(makeSpherical(vertices[getId(0, 0)]));
	boundVertices.push_back(makeSpherical(vertices[getId(0, chunkResolution - 1)]));
	boundVertices.push_back(makeSpherical(vertices[getId(chunkResolution - 1, 0)]));
	boundVertices.push_back(makeSpherical(vertices[getId(chunkResolution - 1, chunkResolution - 1)]));
	boundVertices.push_back(makeSpherical(vertices[getId(chunkResolution / 2, chunkResolution / 2)]));
	
	GLfloat* vertData = chunkVertices.data();
	GLuint* indData = indices.data();
	indicesCount = indices.size();

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * chunkVertices.size(), vertData, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), indData, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	if (level > 0)
	{
		children[0] = make_unique<QuadtreeNode>(resolution, chunkResolution, level - 1, xoff, yoff, indices, vertices, textureCoords);
		children[1] = make_unique<QuadtreeNode>(resolution, chunkResolution, level - 1, xoff + (chunkGap / 2) * chunkResolution, yoff, indices, vertices, textureCoords);
		children[2] = make_unique<QuadtreeNode>(resolution, chunkResolution, level - 1, xoff, yoff + (chunkGap / 2) * chunkResolution, indices, vertices, textureCoords);
		children[3] = make_unique<QuadtreeNode>(resolution, chunkResolution, level - 1, xoff + (chunkGap / 2) * chunkResolution, yoff + (chunkGap / 2) * chunkResolution, indices, vertices, textureCoords);
	}
}

QuadtreeNode::~QuadtreeNode()
{
	if (vao != 0)
		glDeleteVertexArrays(1, &vao);
	if (vbo != 0)
		glDeleteBuffers(1, &vbo);
	if (ebo != 0)
		glDeleteBuffers(1, &ebo);
}

GLfloat QuadtreeNode::distanceFromCamera(glm::vec3 cameraPosition)
{
	GLfloat distance = glm::distance(cameraPosition, boundVertices[0]);
	for (GLuint i = 1; i < boundVertices.size(); ++i) 
	{
		distance = min(distance, glm::distance(cameraPosition, boundVertices[i]));
	}
	return distance;
}

void QuadtreeNode::draw(glm::vec3 cameraPosition)
{
	GLfloat distance = distanceFromCamera(cameraPosition);
	if (lodLevel == 0 || distance > (1 << lodLevel) * LOD_DISTANCE)
	{
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	else
	{
		for (GLuint i = 0; i < 4; ++i)
		{
			children[i]->draw(cameraPosition);
		}
	}
}