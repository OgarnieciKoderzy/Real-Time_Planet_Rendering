#pragma once
#include "Planet.h"

class MovingPlanet : public Planet
{
private:
	GLdouble startXPosition;
	GLdouble startYPosition;
	GLdouble startZPosition;
	long positionIterator = 0;
	GLboolean zToggle = false;
	GLdouble zPositionChange;
	GLdouble rotationTime = 0;
	GLdouble totalRotationTime;
public:

	MovingPlanet(Shader shader, GLuint resolution, GLuint lodLevels, GLfloat totalRotationTime = 1, int colorSet = 0);

	void generatePosition(int max, int min);
	void generateScale(int max);
	void move(GLfloat deltaTime);

	GLdouble getXStartPosition() { return startXPosition; }
	GLdouble getYStartPosition() { return startYPosition; }
	GLdouble getZStartPosition() { return startZPosition; }
};

