#include "MovingPlanet.h"

MovingPlanet::MovingPlanet(Shader shader, GLuint resolution, GLuint lodLevels, GLfloat totalRotationTime, int colorSet) : Planet(shader, resolution, lodLevels, colorSet)
{
	this->totalRotationTime = totalRotationTime;
	this->rotationTime = ((rand() % 100) / 100.0) * totalRotationTime;
}

void MovingPlanet::generatePosition(int max, int min) 
{
	startXPosition = ((float)((rand() % max) + min));
	startYPosition = ((float)((rand() % max) + min));
	startZPosition = ((float)((rand() % 10) - 5));
	if (startZPosition < 0)
		zToggle = true;
	zPositionChange = 2 * abs(startZPosition) / (this->totalRotationTime / 2);
	this->setPosition(glm::vec3(startXPosition, startZPosition, startYPosition));
}

void MovingPlanet::generateScale(int max) 
{
	this->setScale(glm::vec3(((float)(rand() % max))));
}

void MovingPlanet::move(GLfloat deltaTime)
{
	this->rotationTime += deltaTime;
	if (this->rotationTime >= this->totalRotationTime)
		this->rotationTime -= this->totalRotationTime;
	GLdouble newZPos;
	if (this->getPosition()[1] <= -abs(this->getZStartPosition()))
		zToggle = true;
	else if (this->getPosition()[1] >= abs(this->getZStartPosition()))
		zToggle = false;
	if (zToggle)
		newZPos = this->getPosition()[1] + zPositionChange * deltaTime;
	else
		newZPos = this->getPosition()[1] - zPositionChange * deltaTime;
	setPosition(glm::vec3(
		sqrt(pow(getXStartPosition(), 2) + pow(getZStartPosition(), 2) + pow(getYStartPosition(), 2)) * (sin(2 * M_PI * this->rotationTime / this->totalRotationTime)),
		newZPos,
		sqrt(pow(getXStartPosition(), 2) + pow(getZStartPosition(), 2) + pow(getYStartPosition(), 2)) * (cos(2 * M_PI * this->rotationTime / this->totalRotationTime))));
}
