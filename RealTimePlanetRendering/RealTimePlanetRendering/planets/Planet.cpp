#include "Planet.h"

Planet::Planet(Shader shader, GLuint resolution, GLuint lodLevels, PlanetConfig config) : Planet(shader, resolution, lodLevels)
{
	this->config = config;
}

Planet::Planet(Shader shader, GLuint resolution, GLuint lodLevels, int colorSet) : shader(shader)
{
	if (colorSet == 0) {
		config.color0 = glm::vec3(0.0, 0.0, 0.0);
		config.color1 = glm::vec3(0.8, 0.5, 0.3);
		config.color2 = glm::vec3(1.0, 0.9, 0.8);
		config.color3 = glm::vec3(1.0, 1.0, 1.0);
	}

	else if (colorSet == 1) {
		config.color0 = glm::vec3(0.0, 0.0, 0.0);
		config.color1 = glm::vec3(0.3, 0.3, 0.8);
		config.color2 = glm::vec3(0.7, 0.9, 1.0);
		config.color3 = glm::vec3(1.0, 1.0, 1.0);
	}

	else if (colorSet == 2) {
		config.color0 = glm::vec3(0.0, 0.0, 0.0);
		config.color1 = glm::vec3(0.2, 0.5, 0.2);
		config.color2 = glm::vec3(0.5, 0.9, 0.5);
		config.color3 = glm::vec3(1.0, 1.0, 1.0);
	}

	else if (colorSet == 3) {
		config.color0 = glm::vec3(0.0, 0.0, 0.0);
		config.color1 = glm::vec3(0.3, 0.5, 0.4);
		config.color2 = glm::vec3(0.8, 0.9, 0.5);
		config.color3 = glm::vec3(1.0, 1.0, 1.0);
	}

	else if (colorSet == 4) {
		config.color0 = glm::vec3(0.0, 0.0, 0.0);
		config.color1 = glm::vec3(0.5, 0.8, 0.3);
		config.color2 = glm::vec3(0.9, 1.0, 0.8);
		config.color3 = glm::vec3(1.0, 1.0, 1.0);
	}
	config.step1 = 0.33f;
	config.step2 = 0.66f;

	vector<GLuint> indices, invertedIndices;
	vector<glm::vec3> vertices;
	vector<glm::vec2> textureCoords;
	auto makeIndices = [](GLuint resolution, bool invert) {
		vector<GLuint> indices;
		GLuint rowSize = resolution + 1;
		for (GLuint i = 0; i < resolution; ++i)
		{
			for (GLuint j = 0; j < resolution; ++j)
			{
				GLuint id = i * rowSize + j;
				if (invert)
				{
					indices.push_back(id);
					indices.push_back(id + rowSize + 1);
					indices.push_back(id + rowSize);

					indices.push_back(id);
					indices.push_back(id + 1);
					indices.push_back(id + rowSize + 1);
				}
				else
				{
					indices.push_back(id);
					indices.push_back(id + rowSize);
					indices.push_back(id + rowSize + 1);

					indices.push_back(id);
					indices.push_back(id + rowSize + 1);
					indices.push_back(id + 1);
				}
			}
		}
		return indices;
	};

	auto makeGrid = [resolution](vector<glm::vec3>& vertices, vector<glm::vec2>& textureCoords, GLuint wall) {
		GLfloat chunk = 2.0f / resolution;
		GLfloat halfChunk = 1.0f / resolution;
		for (GLuint i = 0; i <= resolution; ++i)
		{
			for (GLuint j = 0; j <= resolution; ++j)
			{
				GLfloat x = chunk * j - 1.0f;
				GLfloat y = chunk * i - 1.0f;
				switch (wall)
				{
				case BACK:
					vertices.push_back(glm::vec3(x, y, -1.0f));
					break;
				case FRONT:
					vertices.push_back(glm::vec3(x, y, 1.0f));
					break;
				case RIGHT:
					vertices.push_back(glm::vec3(1.0f, y, x));
					break;
				case LEFT:
					vertices.push_back(glm::vec3(-1.0f, y, x));
					break;
				case TOP:
					vertices.push_back(glm::vec3(x, 1.0f, y));
					break;
				case BOTTOM:
					vertices.push_back(glm::vec3(x, -1.0f, y));
					break;
				}
				x = halfChunk * j;
				y = halfChunk * i;
				textureCoords.push_back(glm::vec2(x, y));
			}
		}
		return;
	};

	auto makeSpherical = [](vector<glm::vec3>& vertices) {
		for (auto& v : vertices)
		{
			GLfloat dx = v.x * sqrtf(1.0f - (v.y*v.y / 2.0f) - (v.z*v.z / 2.0f) + (v.y*v.y*v.z*v.z / 3.0f));
			GLfloat dy = v.y * sqrtf(1.0f - (v.z*v.z / 2.0f) - (v.x*v.x / 2.0f) + (v.z*v.z*v.x*v.x / 3.0f));
			GLfloat dz = v.z * sqrtf(1.0f - (v.x*v.x / 2.0f) - (v.y*v.y / 2.0f) + (v.x*v.x*v.y*v.y / 3.0f));
			v = glm::vec3(dx, dy, dz);
		}
	};

	GLuint chunkResolution = resolution >> lodLevels;
	indices = makeIndices(chunkResolution, false);
	invertedIndices = makeIndices(chunkResolution, true);
	for (GLuint i = 0; i < 6; ++i)
	{
		makeGrid(vertices, textureCoords, i);
		makeSpherical(vertices);
		walls[i] = make_unique<QuadtreeNode>(resolution, chunkResolution, lodLevels, 0, 0, i % 2 == 0 ? indices : invertedIndices, vertices, textureCoords);
		vertices.clear();
	}
}

void Planet::setHeightMaps(vector<GLuint>& ids, Shader normalizer)
{
	for (int i = 0; i < min((int)ids.size(), 6); ++i)
	{
		heightMaps[i] = ids[i];
		normalMaps[i] = getNormalMap(heightMaps[i], i, normalizer);
	}
}

GLuint Planet::getNormalMap(GLuint heightMap, GLuint face, Shader normalizer)
{
	const GLuint resolution = 300;

	GLuint fbo;
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);

	GLuint normalMap;
	glGenTextures(1, &normalMap);
	glBindTexture(GL_TEXTURE_2D, normalMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, resolution, resolution, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, normalMap, 0);
	glViewport(0, 0, resolution, resolution);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	normalizer.use();
	normalizer.setUniform1f("sampleDistance", 1.0f / (resolution + 1));
	normalizer.setUniform1f("invertedSampleDistance", (resolution + 1) * 0.25f);
	normalizer.setUniformMatrix3fv("faceTransform", getFaceTransform(face));
	normalizer.setTextureUnit("heightMap", heightMap, 0);
	normalizer.setUniform1f("heightScale", heightScale);

	const GLfloat vertices[] = {
		1, 1,		1, 1,		1, -1,
		-1, 1,		0, 1,		-1, -1,
		1, -1,		1, 0,		1, 1,
		-1, -1,		0, 0,		-1, 1
	};

	GLuint vao, vbo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(vao);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(4 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &fbo);

	glViewport(0, 0, WIDTH, HEIGHT);

	return normalMap;
}

glm::mat3 Planet::getFaceTransform(GLuint face)
{
	switch (face) {
	case BACK:
		return glm::mat3(
			1, 0, 0,
			0, 1, 0,
			0, 0, -1);
	case FRONT:
		return glm::mat3(
			1, 0, 0,
			0, 1, 0,
			0, 0, 1);
	case RIGHT:
		return glm::mat3(
			0, 0, 1,
			0, 1, 0,
			1, 0, 0);
	case LEFT:
		return glm::mat3(
			0, 0, 1,
			0, 1, 0,
			-1, 0, 0);
	case TOP:
		return glm::mat3(
			1, 0, 0,
			0, 0, -1,
			0, -1, 0);
	case BOTTOM:
		return glm::mat3(
			1, 0, 0,
			0, 0, -1,
			0, 1, 0);
	default:
		return glm::mat3();
	}
}

void Planet::draw(shared_ptr<Camera> camera, glm::vec3 lightPosition)
{
	glm::vec3 cameraPos = camera->getPosition();
	glm::mat4 model;
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	glm::mat4 view = camera->getViewMatrix();
	glm::mat4 projection = glm::perspective(FOV, (GLfloat)WIDTH / (GLfloat)HEIGHT, Z_NEAR, Z_FAR);

	shader.use();
	shader.setUniform3f("lightPos", lightPosition.x, lightPosition.y, lightPosition.z);
	shader.setUniform3f("viewPos", cameraPos.x, cameraPos.y, cameraPos.z);
	shader.setUniformMatrix4fv("model", model);
	shader.setUniformMatrix4fv("view", view);
	shader.setUniformMatrix4fv("projection", projection);
	shader.setUniform1f("heightScale", heightScale);

	shader.setUniform3f("firstColor", config.color0.r, config.color0.g, config.color0.b);
	shader.setUniform3f("secondColor", config.color1.r, config.color1.g, config.color1.b);
	shader.setUniform3f("thirdColor", config.color2.r, config.color2.g, config.color2.b);
	shader.setUniform3f("fourthColor", config.color3.r, config.color3.g, config.color3.b);
	shader.setUniform1f("firstStep", config.step1);
	shader.setUniform1f("secondStep", config.step2);

	model = glm::inverse(model);
	glm::vec4 cameraMat(cameraPos.x, cameraPos.y, cameraPos.z, 1.0f);
	cameraMat = model * cameraMat;
	cameraPos = glm::vec3(cameraMat.x, cameraMat.y, cameraMat.z);

	for (GLuint i = 0; i < 6; ++i)
	{
		shader.setTextureUnit("heightMap", heightMaps[i], 0);
		shader.setTextureUnit("normalMap", normalMaps[i], 1);
		walls[i]->draw(cameraPos);
	}
}

void Planet::setPosition(glm::vec3 pos)
{
	position = pos;
}

glm::vec3 Planet::getPosition()
{
	return position;
}

void Planet::setScale(glm::vec3 s)
{
	scale = s;
}

glm::vec3 Planet::getScale()
{
	return scale;
}