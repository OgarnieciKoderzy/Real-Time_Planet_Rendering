#pragma once
#include <vector>
#include <algorithm>
#include "common.h"
#include "Camera.h"

const GLfloat LOD_DISTANCE = 0.2f;

class QuadtreeNode
{
private:
	GLuint lodLevel;
	GLuint indicesCount;
	GLuint vbo, vao, ebo;
	unique_ptr<QuadtreeNode> children[4];
	vector<glm::vec3> boundVertices;
public:
	QuadtreeNode(GLuint resolution, GLuint chunkResolution, GLuint level, GLuint xoff, GLuint yoff, vector<GLuint>& indices, vector<glm::vec3>& vertices, vector<glm::vec2>& textureCoords);
	~QuadtreeNode();

	GLfloat distanceFromCamera(glm::vec3 cameraPosition);
	void draw(glm::vec3 cameraPosition);
};

