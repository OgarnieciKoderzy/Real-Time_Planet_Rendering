#include "Sun.h"


Sun::Sun(Shader shader) : shader(shader)
{
	GLfloat quadVertices[] = {
		-1.0f, 1.0f, -0.5f, 0.5f,
		-1.0f, -1.0f, -0.5f, -0.5f,
		1.0f, -1.0f, 0.5f, -0.5f,

		-1.0f, 1.0f, -0.5f, 0.5f,
		1.0f, -1.0f, 0.5f, -0.5f,
		1.0f, 1.0f, 0.5f, 0.5f
	};

	GLuint vbo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glBindVertexArray(0);
}

void Sun::draw(shared_ptr<Camera> camera, glm::vec3 lightPosition)
{
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::vec3 cameraPos = camera->getPosition();
	glm::mat4 model;
	model = glm::translate(model, position);
	glm::mat4 view = camera->getViewMatrix();
	glm::mat4 projection = glm::perspective(FOV, (GLfloat)WIDTH / (GLfloat)HEIGHT, Z_NEAR, Z_FAR);
	glm::mat4 modelView = view * model;
	for (int i = 0; i < 3; ++i) for(int j = 0; j < 3; ++j) modelView[i][j] = i == j ? 1.0f : 0.0f;
	modelView = glm::scale(modelView, scale);

	shader.use();
	shader.setUniformMatrix4fv("projection", projection);
	shader.setUniformMatrix4fv("modelView", modelView);
	shader.setUniform1f("time", (GLfloat)(glfwGetTime() * 1));

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}

void Sun::setPosition(glm::vec3 pos)
{
	position = pos;
}

glm::vec3 Sun::getPosition()
{
	return position;
}

void Sun::setScale(glm::vec3 s)
{
	scale = s;
}

glm::vec3 Sun::getScale()
{
	return scale;
}