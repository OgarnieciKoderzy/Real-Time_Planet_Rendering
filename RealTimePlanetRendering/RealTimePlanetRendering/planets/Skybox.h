#pragma once
#include <vector>
#include <SOIL.h>
#include "common.h"
#include "Shader.h"
#include "Camera.h"
#include "Texture.h"

class Skybox
{
private:
	GLuint vao;
	GLuint texture;
	Shader shader;

	GLuint loadCubemap(vector<string> faces);
public:
	Skybox(Shader shader);

	void draw(shared_ptr<Camera> camera);
};

