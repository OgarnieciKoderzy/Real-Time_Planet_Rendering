 #pragma once
#include "Shader.h"
#include "Camera.h"
#include "QuadtreeNode.h"
#include "Texture.h"

const GLfloat heightScale = 0.1f;

enum PlanetFaces { BACK = 0, FRONT, RIGHT, LEFT, TOP, BOTTOM };

struct PlanetConfig
{
	glm::vec3 color0, color1, color2, color3;
	GLfloat step1, step2;
};

class Planet
{
private:
	PlanetConfig config;
	Shader shader;
	glm::vec3 position;
	glm::vec3 scale;
	unique_ptr<QuadtreeNode> walls[6];
	GLuint heightMaps[6], normalMaps[6];

	glm::mat3 getFaceTransform(GLuint face);
	GLuint getNormalMap(GLuint heightMap, GLuint face, Shader normalizer);
public:
	Planet(Shader shader, GLuint resolution, GLuint lodLevels, PlanetConfig config);
	Planet(Shader shader, GLuint resolution, GLuint lodLevels, int colorSet = 0);
	void setHeightMaps(vector<GLuint>& ids, Shader normalizer);

	void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition);
	void setPosition(glm::vec3 pos);
	glm::vec3 getPosition();
	void setScale(glm::vec3 s);
	glm::vec3 getScale();
};