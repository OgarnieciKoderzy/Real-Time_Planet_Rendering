#pragma once
#include "AssetsManager.h"
#include "MovingPlanet.h"
#include "Sun.h"
#include "HeightMapTexture.h"

const GLint MAX_PLANET_SIZE = 10;
const GLint MAX_POSITION = 20;
const GLint ORBITS_NO = 9;
const GLint ORBIT_SIZE = 60;

class SolarSystem
{
private:
	vector<shared_ptr<MovingPlanet>> planets;
	unique_ptr<Sun> sun;
public:
	SolarSystem(shared_ptr<AssetsManager> assets);

	void draw(shared_ptr<Camera> camera);
	void update(GLdouble deltaTime);
	void checkCollision(shared_ptr<Camera> camera);

	weak_ptr<Planet> getClosestPlanet(glm::vec3 pos);
};

