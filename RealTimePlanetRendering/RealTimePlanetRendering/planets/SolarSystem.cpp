#include "SolarSystem.h"

SolarSystem::SolarSystem(shared_ptr<AssetsManager> assets)
{
	// initialize galaxy
	sun = make_unique<Sun>(assets->getShader("sun"));
	sun->setPosition(glm::vec3(0, 0, 0));
	sun->setScale(glm::vec3(3 * MAX_PLANET_SIZE));


	for (int i = 0; i < ORBITS_NO; i++) {
		vector<GLuint> heightMaps;
		for (int i = 0; i < 6; i++) {
			srand(std::rand());
			HeightMapTexture map((float)std::rand() / RAND_MAX * 2 - 1.0f, (float)std::rand() / RAND_MAX * 2 - 1.0f, 256, 256);
			GLuint texture = map.getTexture();
			heightMaps.push_back(texture);
		}
		shared_ptr<MovingPlanet> planet = make_shared<MovingPlanet>(assets->getShader("terrain"), 256, 4, (GLfloat)((rand() % 40) + 20)*(i + 1), rand()%5);
		planet->generatePosition(ORBIT_SIZE * (i + 1), (i)* ORBIT_SIZE + 2 * MAX_PLANET_SIZE + 5);
		planet->generateScale(MAX_PLANET_SIZE);
		planet->setHeightMaps(heightMaps, assets->getShader("normal"));

		planets.push_back(planet);
	}
}

void SolarSystem::draw(shared_ptr<Camera> camera)
{
	auto lightPosition = sun->getPosition();

	for (auto const& it : planets) 
	{
		it->draw(camera, lightPosition);
	}
	sun->draw(camera, lightPosition);
}

void SolarSystem::update(GLdouble deltaTime)
{
	for (auto const& it : planets)
	{
		it->move((GLfloat)deltaTime);
	}
}

weak_ptr<Planet> SolarSystem::getClosestPlanet(glm::vec3 pos)
{
	auto closest = *(planets.begin());
	for (auto it : planets)
	{
		if (glm::distance(it->getPosition(), pos) < glm::distance(closest->getPosition(), pos))
			closest = it;
	}
	return weak_ptr<Planet>(closest);
}

void SolarSystem::checkCollision(shared_ptr<Camera> camera)
{
	for (auto it : planets)
	{
		if (glm::distance(it->getPosition(), camera->getPosition()) < it->getScale().x * (1.0f + heightScale / 2))
		{
			auto pos = camera->getPosition() - it->getPosition();
			pos = glm::normalize(pos);
			pos *= it->getScale().x * (1.0f + heightScale / 2);
			pos += it->getPosition();
			camera->setPosition(pos);
		}
	}
	if (glm::distance(sun->getPosition(), camera->getPosition()) < sun->getScale().x)
	{
		auto pos = camera->getPosition();
		pos = glm::normalize(pos);
		pos *= sun->getScale().x;
		camera->setPosition(pos);
	}
}