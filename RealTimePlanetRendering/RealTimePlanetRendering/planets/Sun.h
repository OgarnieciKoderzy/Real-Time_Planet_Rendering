#pragma once
#include "Planet.h"

class Sun
{
private:
	Shader shader;
	GLuint vao;
	glm::vec3 position;
	glm::vec3 scale;
public:
	Sun(Shader shader);

	void draw(shared_ptr<Camera> camera, glm::vec3 lightPosition);
	void setPosition(glm::vec3 pos);
	glm::vec3 getPosition();
	void setScale(glm::vec3 s);
	glm::vec3 getScale();
};